# Purdue UAS Flight Areas

<iframe src="https://www.google.com/maps/d/embed?mid=1aUV3dlW0xSuJC1D6onC3ete3Uz1RVe0q&ehbc=2E312F" width="640" height="480"></iframe>
[Link to map](https://www.google.com/maps/d/viewer?mid=1aUV3dlW0xSuJC1D6onC3ete3Uz1RVe0q&ll=40.41234212100329%2C-86.95216865000002&z=12)

## Purdue Turf Farm

## Purdue Wildlife Area

## Martell Forest

* * *

👋 Notice something off? Want to make a change? Submit an [issue](https://gitlab.com/purdue-uas/dispatch/public/-/issues/new), email <a href="mailto:UASdispatch@purdue.edu, nathanrose@purdue.edu">UAS Dispatch Team</a>, or [fix it](https://gitlab.com/purdue-uas/dispatch/public) yourself.