# Anemometer Comparison Array

<center>
  <img src="../img/anemometer_array_full.jpg" width="100%" alt="Picture of Anemometer Comparison Array">
</center>

## Introduction
On 17 December 2021, a anemometer comparison array was installed on the weather tower at Innisfree Village to compare anemometer performance between [RM Young 85000](https://www.youngusa.com/wp-content/uploads/2020/01/85000-90J.pdf), [Trisonica Mini](https://anemoment.com/wp-content/uploads/2021/07/TSM-User-Manual-2021-07-09.pdf), [Calypso ULP485](https://calypsoinstruments.com/web/content/7091?access_token=6b740c33-1751-4d9a-84d2-b2c7c1aa67ee&unique=undefined&download=true), [Tempest](https://help.weatherflow.com/hc/en-us/categories/203614828-Tempest-Weather-System), and [ATMOS41](http://library.metergroup.com/Manuals/20635_ATMOS41_Manual_Web.pdf). The following information describes the installation, connections, and hardware/software configurations.

## Wiring
There are three dataloggers in the Campbell Scientific weatherproof box: two [CR1000s](https://s.campbellsci.com/documents/us/manuals/cr1000.pdf) and one [Arduino Mega](https://www.arduino.cc/en/pmwiki.php?n=Main/ArduinoBoardMega). The wiring is broken down by the two CR1000 dataloggers.

### Upper CR1000 Wiring

#### RM Young 85000
| Signal Description | Extension Wire Color | Instrument Wire Color |  CR1000 Pin  |
|:------------------:|:--------------------:|:---------------------:|:------------:|
|       12V_PWR      |          Red         |          Red          |      12V     |
|      GND_EARTH     |         Bare         |          Bare         |       G      |
|       GND_PWR      |         Black        |         Black         |       G      |
|      RS232_TX      |         Brown        |         Brown         |    COM1_C2   |
|      RS232_RX      |         Green        |         Green         | NC (COM1_C1) |
|         REF        |         White        |         White         |      NC      |

#### Trisonica Mini
| Signal Description | Extension Wire Color | Instrument Wire Color |  CR1000 Pin  |
|:------------------:|:--------------------:|:---------------------:|:------------:|
|       12V_PWR      |          Red         |         Yellow        |      12V     |
|       GND_PWR      |         Brown        |         Black         |       G      |
|      RS232_TX      |        Orange        |          Red          |    COM2_C4   |
|      RS232_RX      |         White        |         Green         | NC (COM2_C3) |

#### Calypso ULP485
| Signal Description | Extension Wire Color | Instrument Wire Color | CR1000 Pin | Arduino Mega Pin |
|:------------------:|:--------------------:|:---------------------:|:----------:|:----------------:|
|       12V_PWR      |         Brown        |         Brown         |     12V    |                  |
|       GND_PWR      |         White        |         White         |      G     |                  |
|      RS485_A+      |         Green        |         Green         |            |     RX1 (19)     |
|      RS485_B-      |        Orange        |         Yellow        |            |     TX1 (18)     |
|       5V_PWR       |           -          |         Orange        |     5V     |        VIN       |
|       GND_PWR      |           -          |         White         |      G     |        GND       |
|      RS232_TX      |                      |         Orange        |   COM4_C8  |     TX2 (16)     |
|      RS232_RX      |                      |         Yellow        |   COM4_C7  |     RX2 (17)     |

#### WeatherFlow Tempest
| Signal Description | Extension Wire Color |   Instrument Wire Color  | CR1000 Pin |
|:------------------:|:--------------------:|:------------------------:|:----------:|
|       12V_PWR      |           -          |   USB Adapter In – Red   |     12V    |
|       GND_PWR      |           -          |  USB Adapter In – Black  |      G     |
|    5V_USB_MICRO    |           -          | USB Adapter Out – To Hub |            |
|     GND_SIGNAL     |           -          |           Black          |      G     |
|      RS232_TX      |           -          |            Red           |   COM3_C6  |

### Lower CR1000 Wiring

#### ATMOS41
| Signal Description | Extension Wire Color | Instrument Wire Color | CR1000 Pin |
|:------------------:|:--------------------:|:---------------------:|:----------:|
|       12V_PWR      |           -          |         Brown         |     12V    |
|       GND_PWR      |           -          |          Bare         |      G     |
|        SDI12       |           -          |         Orange        |   COM4_C7  |

## Code
The CR1000s run CRBasic code. This can best be edited using the [CRBasic Editor](https://www.campbellsci.com/crbasiceditor) which can be downloaded with [PC400](https://www.campbellsci.com/downloads/pc400) after [registering](https://www.campbellsci.com/register) for an account.

The Arduino can be programmed using the [Arduino IDE](https://www.arduino.cc/en/software) either in the [online editor](https://create.arduino.cc/editor) or [desktop IDE](https://www.arduino.cc/en/Main/Software).

### Upper CR1000 Code: [GitLab Link](https://gitlab.com/de-wekker-lab/anemometer-comparison-array/-/blob/main/crbasic-code/Innisfree_UpperCR1000_RMYoung_Trisonica_Calypso_Tempest_20211115.CR1)

### Lower CR1000 Wiring: [GitLab Link](https://gitlab.com/de-wekker-lab/anemometer-comparison-array/-/blob/main/crbasic-code/Innisfree_LowerCR1000_ATMOS41_20211112.CR1)

### Arduino Mega Code: [GitLab Link](https://gitlab.com/de-wekker-lab/anemometer-comparison-array/-/tree/main/arduino-code)

***

# Tasks and Nathan's Notes
- Need to add 12v regulator in line with the solar panel.
- Need to buy new set screw and possible magnet for second RM 05103
- Need wires for connecting power to CR1000s (make this with the 2C wire in G094)
- Need a [two pin plug](https://www.campbellsci.com/p3768) for powering second CR1000 (the green plug)
- Need to loosen ground lug and install earth ground to box and to CR1000s
- Consider changing the 'BufferOption' to 1000 in the 'Innisfree_UpperCR1000_RMYoung_Trisonica_Calypso_Tempest_20211115.CR1' code. `Scan(20,mSec,1000,0)`
