# UAS Registration Certificates
*The registration certificates listed below should be used as a secondary resource to the paper registration certificate provided with the aircraft.*

# Aircraft
## [DJI Mavic 2 Pro](#m2p)


* * *

## <a name="m2p"></a>DJI Mavic 2 Pro
Aircraft: [A](#m2pA) | [B](#m2pB) | [C](#m2pC) | [D](#m2pD) | [E](#m2pE) | [F](#m2pF) | [G](#m2pG) | [H](#m2pH) | <s>[AA](#m2pAA)</s> | [AB](#m2pAB) | [AC](#m2pAC) | [AD](#m2pAD) | [AE](#m2pAE) | [AF](#m2pAF) | [AG](#m2pAG) | [AH](#m2pAH) | [AI](#m2pAI) | [AJ](#m2pAJ) | [AK](#m2pAK) | [AL](#m2pAL)

### <a name="m2pA"></a>DJI Mavic 2 Pro A
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_A.pdf" width="100%" height="250px"></iframe>


### <a name="m2pB"></a>DJI Mavic 2 Pro B
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_B.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="m2pC"></a>DJI Mavic 2 Pro C
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_C.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="m2pD"></a>DJI Mavic 2 Pro D
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_D.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="m2pE"></a>DJI Mavic 2 Pro E
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_E.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="m2pF"></a>DJI Mavic 2 Pro F
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_F.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="m2pG"></a>DJI Mavic 2 Pro G
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_G.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="m2pH"></a>DJI Mavic 2 Pro H
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_H.pdf" width="100%" height="250px"></iframe>
* * *

### <s><a name="m2pAA"></a>DJI Mavic 2 Pro AA</s>
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_AA.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="m2pAB"></a>DJI Mavic 2 Pro AB
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_AB.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="m2pAC"></a>DJI Mavic 2 Pro AC
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_AC.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="m2pAD"></a>DJI Mavic 2 Pro AD
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_AD.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="m2pAE"></a>DJI Mavic 2 Pro AE
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_AE.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="m2pAF"></a>DJI Mavic 2 Pro AF
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_AF.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="m2pAG"></a>DJI Mavic 2 Pro AG
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_AG.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="m2pAH"></a>DJI Mavic 2 Pro AH
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_AH.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="m2pAI"></a>DJI Mavic 2 Pro AI
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_AI.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="m2pAJ"></a>DJI Mavic 2 Pro AJ
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_AJ.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="m2pAK"></a>DJI Mavic 2 Pro AK
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_AK.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="m2pAL"></a>DJI Mavic 2 Pro AL
<iframe src="/dispatch/public/resources/uasreg/DJI_Mavic2_AL.pdf" width="100%" height="250px"></iframe>

* * *

## <a name="skydio2+"></a>Skydio 2+
Aircraft: [A](#skydio2+A) | [B](#skydio2+B) | [C](#skydio2+C) | [D](#skydio2+D) | [E](#skydio2+E) | [F](#skydio2+F) | [G](#skydio2+G) | [H](#skydio2+H) | [I](#skydio2+I) | [J](#skydio2+J)

### <a name="skydio2+A"></a>Skydio 2+ A
<iframe src="/dispatch/public/resources/uasreg/Skydio_S2+_A.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="skydio2+B"></a>Skydio 2+ B
<iframe src="/dispatch/public/resources/uasreg/Skydio_S2+_B.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="skydio2+C"></a>Skydio 2+ C
<iframe src="/dispatch/public/resources/uasreg/Skydio_S2+_C.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="skydio2+D"></a>Skydio 2+ D
<iframe src="/dispatch/public/resources/uasreg/Skydio_S2+_D.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="skydio2+E"></a>Skydio 2+ E
<iframe src="/dispatch/public/resources/uasreg/Skydio_S2+_E.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="skydio2+F"></a>Skydio 2+ F
<iframe src="/dispatch/public/resources/uasreg/Skydio_S2+_F.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="skydio2+G"></a>Skydio 2+ G
<iframe src="/dispatch/public/resources/uasreg/Skydio_S2+_G.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="skydio2+H"></a>Skydio 2+ H
<iframe src="/dispatch/public/resources/uasreg/Skydio_S2+_H.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="skydio2+I"></a>Skydio 2+ I
<iframe src="/dispatch/public/resources/uasreg/Skydio_S2+_I.pdf" width="100%" height="250px"></iframe>
* * *

### <a name="skydio2+J"></a>Skydio 2+ J
<iframe src="/dispatch/public/resources/uasreg/Skydio_S2+_J.pdf" width="100%" height="250px"></iframe>
* * *

## <a name="m210"></a>DJI Matrice 210
Aircraft: [A](#m210A) | [B](#m210B) 

### <a name="m210A"></a>DJI Matrice 210 A
<iframe src="/dispatch/public/resources/uasreg/DJI_M210_A.pdf" width="100%" height="250px"></iframe>
* * *
### <a name="m210B"></a>DJI Matrice 210 B
<iframe src="/dispatch/public/resources/uasreg/DJI_M210_B.pdf" width="100%" height="250px"></iframe>
* * *

## <a name="m300"></a>DJI Matrice 300
Aircraft: [A](#m300A) | [B](#m300B) | [C](#m300C) | [D](#300D)

### <a name="m300A"></a>DJI Matrice 300 A
<iframe src="/dispatch/public/resources/uasreg/DJI_M300_A.pdf" width="100%" height="250px"></iframe>
* * *
### <a name="m300B"></a>DJI Matrice 300 B
<iframe src="/dispatch/public/resources/uasreg/DJI_M300_B.pdf" width="100%" height="250px"></iframe>
* * *
### <a name="m300C"></a>DJI Matrice 300 C
<iframe src="/dispatch/public/resources/uasreg/DJI_M300_C.pdf" width="100%" height="250px"></iframe>
* * *
### <a name="m300D"></a>DJI Matrice 300 D
<iframe src="/dispatch/public/resources/uasreg/DJI_M300_D.pdf" width="100%" height="250px"></iframe>
* * *

## <a name="bramor"></a>C-Astral Bramor ppX
Aircraft: [A](#bramorA) | [B](#bramorB)

### <a name="bramorA"></a>C-Astral Bramor ppX A
<iframe src="/dispatch/public/resources/uasreg/C-Astral_Bramor_ppX_1.pdf" width="100%" height="250px"></iframe>
* * *
### <a name="bramorB"></a>C-Astral Bramor ppX B
<iframe src="/dispatch/public/resources/uasreg/C-Astral_Bramor_ppX_2.pdf" width="100%" height="250px"></iframe>
* * *


## <a name="custom"></a>Custom Build
Aircraft: [A]() | [B]() 


* * *

👋 Notice something off? Want to make a change? Submit an [issue](https://gitlab.com/purdue-uas/dispatch/public/-/issues/new), email <a href="mailto:UASdispatch@purdue.edu, nathanrose@purdue.edu">UAS Dispatch Team</a>, or [fix it](https://gitlab.com/purdue-uas/dispatch/public) yourself.
