# Contact Us

**Email:** [UASdispatch@purdue.edu](mailto:UASdispatch@purdue.edu?subject=Website%20Contact)

**Phone:** [765.496.3229](tel:765-496-322)

<p>

<b>Location:</b> COMP 101
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1807.5776479739347!2d-86.9298886!3d40.4159454!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8812e2c475f239af%3A0xa31d99a3961ec9e6!2sComposites%20Laboratory%20C11%2C%20West%20Lafayette%2C%20IN%2047906!5e1!3m2!1sen!2sus!4v1650153688871!5m2!1sen!2sus" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe></p>

* * *

👋 Notice something off? Want to make a change? Submit an [issue](https://gitlab.com/purdue-uas/dispatch/public/-/issues/new), email <a href="mailto:UASdispatch@purdue.edu, nathanrose@purdue.edu">UAS Dispatch Team</a>, or [fix it](https://gitlab.com/purdue-uas/dispatch/public) yourself.