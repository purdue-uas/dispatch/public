# Weather Sources

## KLAF METAR & TAF
!!! note "KLAF METAR"

    <div data-type="METAR" data-station="KLAF"></div>

!!! note "KLAF TAF"

    <div data-type="TAF" data-station="KLAF"></div>

## Aviation Weather Center (AWC)

> **[AWC Homepage](https://aviationweather.gov)**

> **[AWC KLAF](https://aviationweather.gov/data/metar/?id=KLAF&hours=0&decoded=yes&include_taf=yes)**

## Windy Forecast
<iframe width="100%" height="750" src="https://embed.windy.com/embed.html?type=map&location=coordinates&metricRain=default&metricTemp=default&metricWind=default&zoom=8&overlay=wind&product=ecmwf&level=surface&lat=40.032&lon=-86.885&detailLat=40.409&detailLon=-86.886&detail=true&pressure=true" frameborder="0"></iframe>

<script src="https://api.checkwx.com/widget?key=d64fa9e364c5499d867a9bbdf5" type="text/javascript"></script>