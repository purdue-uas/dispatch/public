# E mergent RC Intense Eye V2 Drone

<center>
  <img src="../img/IEV2_Full.jpg" width="100%" alt="IEV2 UAS with WP_V2 Attached">
</center>

## Introduction
<iframe src="https://www.wibotic.com/wp-content/uploads/2021/05/UAS-Intense-Eye-V2.pdf#toolbar=0" width="100%" height="900"></iframe>

## Manual
IEV2 Manual available on [GitLab](https://gitlab.com/de-wekker-lab/emergentrc-intense-eye-v2/-/blob/master/Manuals/EMergentRC_Intense%20Eye_V2_Information_V1_0.pdf)

## Backup Configurations
The following will guide the user to create backup images of both the transmitter as the UAS. These are available on [GitLab](https://gitlab.com/de-wekker-lab/emergentrc-intense-eye-v2/-/tree/master/Configuration%20Files)

### FrSky Taranis X9D Plus Controller
1. Download and install [OpenTX](https://www.open-tx.org/downloads.html)
2. Power on controller and connect to PC via USB.
3. Using rotary dial on controller select "USB Storage (SD)."
4. Follow the prompts to create a new radio profile (also accessible through Settings > Radio Profiles > Add Radio Profile).

<center>
  <img src="../img/emergentrc_radioProfile.png" width="502" alt="Add Radio Profile" />
</center>

5. Read controller settings by selecting Read/Write > Read Models and Settings from Radio. Verify model name: "IEV2 WX Stn"

<center>
  <img src="../img/emergentrc_readModelsSettings.png" width="307" alt="Read Models and Settings">
</center>

6. Backup firmware by selecting Read/Write > Read Firmware from Radio. Stored on [GitLab](https://gitlab.com/de-wekker-lab/emergentrc-intense-eye-v2/-/blob/master/Configuration%20Files/IEV2_Controller_FirmwareBackup.bin)

<center>
  <img src="../img/emergentrc_backupFirmware.png" width="307" alt="Read Models and Settings">
</center>

7. Backup settings/models by selecting Read/Write > Backup Radio to File. Stored on [GitLab](https://gitlab.com/de-wekker-lab/emergentrc-intense-eye-v2/-/blob/master/Configuration%20Files/IEV2_Controller_SettingsBackup.bin)

<center>
  <img src="../img/emergentrc_backupRadio.png" width="307" alt="Read Models and Settings">
</center>

### E mergent RC Intense Eye V2 CubePilot

A current backup of the parameters on the IEV2 are available on [GitLab](https://gitlab.com/de-wekker-lab/emergentrc-intense-eye-v2/-/blob/master/Configuration%20Files/IEV2_Pixhawk_ParametersBackup.param)

1. Install two antenna to the wireless transmitter in front of the UAS.

<center>
  <img src="../img/IEV2_Antenna.jpg" width="307" alt="IEV2 Antenna Installation">
</center>

2. With propellers removed, power UAS with supplied 6S LiHv battery.
3. Connect the UAS to [MissionPlanner](https://ardupilot.org/planner/) or [QGroundControl](http://qgroundcontrol.com/) by connecting the wireless transmitter (labeled for IEV2) to the PC using a microUSB cable. You may have to select the transmitter in the program options. Parameters will load when connection is established.
4. In MissionPlanner, select "Config/Tuning" to view all parameters. Choose to save current parameters. These will be written to a .param file.
5. In QGroundControl, select the "Q" icon in the upper left and select "Parameters." From the tools menu in the upper right choose "Save to file..." to save a .param file. Review these instructions on the [QGroundControl website](https://docs.qgroundcontrol.com/master/en/SetupView/Parameters.html#tools)

## Configurations and Modifications

### FrSky Taranis X9D Plus Controller Settings

The X9D Plus has two configurations preprogrammed; Mode 1 and Mode 3, labeled as "IEV2 WX Stn" and "IEV2 M3" respectively.

Both of these configurations have the following mode settings using two labeled switches the top right on the transmitter.

<center>
  <img src="../img/IEV2_Controller_SwitchModes.jpg" width="307" alt="Transmitter Switch Modes">
</center>

Switch SC will select between three flight modes:

1. Stabilize: manual flight (no altitude or position hold)
2. Loiter: GPS lock mode (altitude and position hold)
3. Auto: Perform the programmed flight pattern. See [Planning a Mission in QGroundControl](#planning-a-mission-in-qgroundcontrol)

Switch SG will select Return to Launch (RTL) no matter which position (1, 2, or 3) it is switched to. This will return the drone to it's launch position. Note that the drone may jump to a "safe" altitude and yaw towards the launch location as part of the RTL routine.


### Here+ GNSS RTK

The IEV2 currently makes use of the Here+ GPS as a stand-alone device. The steps below largely follow the [Here+ User Manual](https://docs.cubepilot.org/user-guides/here+/here+v2-user-manual) on the CupePilot website according to the current hardware and firmware on the IEV2.

#### [LED Reference](https://docs.cubepilot.org/user-guides/here+/here+v2-user-manual#here-here-v2-led-meaning)
**Flashing red and blue:** Initializing sensors. Place the vehicle still and level while it initializes the sensors.<br>
**Flashing blue:** Disarmed, no GPS lock. Auto-mission, loiter and return-to-launch flight modes require GPS lock<br>
**Solid blue:** Armed with no GPS lock<br>
**Flashing green:** Disarmed (ready to arm), GPS lock acquired. Quick double tone when disarming from the armed state.<br>
**Fast Flashing green:** Same as above but GPS is using SBAS (so should have better position estimate)<br>
**Solid green:** with single long tone at time of arming: Armed, GPS lock acquired. Ready to fly!<br>
**Double flashing yellow:** Failing pre-arm checks (system refuses to arm)<br>
**Single Flashing yellow:** Radio failsafe activated<br>
**Flashing yellow - with quick beeping tone:** Battery failsafe activated<br>
**Flashing yellow and blue- with high-high-high-low tone sequence (dah-dah-dah-doh):** GPS glitch or GPS failsafe activated<br>
**Flashing red and yellow:** EKF or Inertial Nav failure<br>
**Flashing purple and yellow:** Barometer glitch Solid Red: Error，usually due to the SD card（re-plug or place the SD card to solve）,MTD or IMU，you may check the SD card and have a look at BOOT.txt for boot message analysis<br>
**Solid red with SOS tone sequence:** SD card missing or SD card bad format<br>
**No light when power on:** No firmware，firmware lost，SD card missing or bad format（ac3.4 or higher version

#### [Updating Here+ Firmware](https://docs.cubepilot.org/user-guides/here-2/updating-here-2-firmware)
1. Download [Ublox U-center](https://www.u-blox.com/en/product/u-center)
2. Verify version by clicking Receiver > UBX-MON-VER. The Rover and Base have been verified as of 13 May 2021 to be running firmware version 1.40. No additional steps needed.

#### Step-By-Step Guide to Add RTK Feature using [QGroundControl](http://qgroundcontrol.com/)
1. Enter QGroundControl Application Settings by clicking the QGC icon in the upper left and then clicking Application Settings.
2. Under the **RTK GPS** heading choose "Perform Survey-In." Then enter the accuracy in meters, and the minimum observation time. the defaults, 2.00m and 180secs, should suffice.
3. The RTK base station should be mounted above the ground such as on a tripod for greatest accuracy.
4. Once connected, the QGC will begin the survey-in process and will automatically stream position data to the flight controller through the standard wireless telemetry.

### Planning a Mission in QGroundControl
Planning a mission using QGC is simple. Information on the available commands can be found on the ArduPilot website [here](https://ardupilot.org/copter/docs/common-mavlink-mission-command-messages-mav_cmd.html) and [here](https://ardupilot.org/copter/docs/mission-command-list.html#mission-command-list). Plans require, at a minimum, a take off command an a return to launch command. Below is a how to plan a simple vertical sampling mission.

1. Click the **Plan** button in QGC.
2. To create a plan, click on the **File** button and choose **Blank**.
3. Leave the default values in the _Mission Start_ panel.
4. Click the **Waypoint** button and click on the map to add a waypoint.
5. Click the dropdown menu in the _Waypoint_ panel to select additional waypoint commands. From the _All Commands_ category choose _Set launch location_.
6. In the _Set launch location_ panel, set **Altitude = 0, Mode = Vehicle position** to set the launch location. (This is not a necessary step, but mitigates any risk that the flight controller has an incorrect launch location).
7. Click the **Takeoff** button and choose a point on the map (this should be close to the anticipated location for planning purposes, but it set by the flight controller at the start of the mission). In the _Takeoff_ panel, set **Altitude = 5**.
8. Click the **Waypoint** button and click on the map to add a waypoint.
9. Click the dropdown menu in the _Waypoint_ panel to select additional waypoint commands. From the _Conditionals_ category choose _Wait for yaw_.
10. In the _Wait for yaw_ panel, set **Direction = Clockwise, Offset = Absolute, Heading = 0.0, Rate = 5.0**.
11. Click the **Waypoint** button and click on the map to add a waypoint.
12. Click the dropdown menu in the _Waypoint_ panel to select additional waypoint commands. From the _Loiter_ category choose _Loiter (time)_.
13. In the _Loiter (time)_ panel, set **Altitude = 10, Loiter Time = 240**.
14. In the _Loiter (time)_ panel, click the burger bar and select _Show all values_. Set **Lat/X = 0, Lon/Y = 0** and keep the other values the same as they were set in the simple panel view.
15. Continue these steps until all of the horizontal sampling heights have been met.
16. Click the **Return** button. This will end the mission.
17. Click the **File** button and _Save As_ to save the mission for later use.
18. Upload the mission to a connected flight controller by clicking the pulsating upload button in the upper right (only visible when a vehicle is connected).

This mission plan will use the default parameter values for ascent speed and RTL altitude. Below is how to adjust these two parameters to meet the requirements for this mission. Note: Flight controller parameters should be backed up prior to adjusting, see [backup configuration instructions](#e-mergent-rc-intense-eye-v2-cubepilot).

1. With the vehicle connected, enter QGroundControl Vehicle Setup by clicking the QGC icon in the upper left and then clicking Vehicle Setup.
1. Select _Parameters_.
1. Search for _WPNAV_SPEED_UP_ and set the value to 100.
1. Search for _RTL_ALT_ and set the value to 0.
1. Reboot the vehicle and verify that the parameters have been updated.

## Operations
The following will guide the user through the setup and manual flight of the IEV2 UAS.

### Battery Charging
This system uses high current, LiHv batteries requiring a special charger. We have selected the [ISDT P30 charger](https://www.isdt.co/down/pdf/P30_EN.pdf) powered by a 24V, 600W TDK-Lambda AC/DC power supply model [HWS600-24](https://product.tdk.com/system/files/dam/doc/product/power/switching-power/ac-dc-converter/catalog/hws300-1500_e.pdf). This supply falls short of the required 1000W power supply for the P30, but will suffice so long as the power demand is not exceeded. (i.e. no more than 24A for a 6S LiHv battery).

**Connecting the charger:**

1. Connect XT-90 plug from AC/DC power supply to ISDT charger.

<center>
  <img src="../img/ISDT_XT-90.jpg" width="307" alt="XT-90 Plug from AC/DC to ISDT">
</center>

2. Power AC/DC power supply.
3. Plug XT-60 plug from battery into ISDT charger.
4. Plug balance plug from batter into ISDT charger.

<center>
  <img src="../img/ISDT_Balance.jpg" width="307" alt="XT-60 and balance plug from ISDT to battery">
</center>

**Options:**

- Charge/Balance: Hold center touch key to bring up task setting menu

  1. Task: Charge
  2. Battery: LiHv (or LiPo)
  3. Battery and Cell Count: LiHv (6S) (or LiPo (6S))
  4. Voltage: 4.35V (or 4.20V)
  5. Current: 13A (MAX)

- *Storage: Hold center touch key to bring up task setting menu

  1. Task: Storage
  2. Battery: LiHv (of LiPo)
  3. Battery and Cell Count: LiHv (6S) (or LiPo (6S))
  4. Voltage: 3.80V

  *Batteries should be stored at 3.80V for longterm (over 2 weeks) storage. Storing above 3.80V will reduce battery capacity over time.

**Transmitter Battery**

<center>
  <img src="../img/TX_Battery.jpg" width="307" alt="TX 2S LiPo Battery">
</center>

This battery is a 2S LiPo battery that may be charged (and stored) as any generic 2S LiPo using the [SkyRC iMAX B6AC V2 battery charger](https://www.skyrc.com/iMAX_B6AC_V2_Charger) or similar.

### Connections/Installations

The following connections and installations should be made before flight. Note that some connections/installations are optional.

#### Install Battery
The battery installs to the bottom of the UAS.

1. Loosen the velcro straps.
2. Insert the battery through the straps.
3. Center the battery on the velcro pad and seat it fully.
4. Strap the battery in securely with both straps.

<center>
  <img src="../img/IEV2_BatteryInstallation.jpg" width="307" alt="IEV2 Battery Installation">
</center>

When ready for flight, the XT-60 adapter plugs into the top of the UAS.

#### Install Propellers
The UAS comes with two different propellers that must be installed correctly.

1. Inspect the motors for free movement.
2. Install the small adapter ring into the center hole of each propeller. This ring is critical to centering the propeller on the motor.

<center>
  <img src="../img/IEV2_AdapterRing.jpg" width="307" alt="IEV2 Propeller Adapter Ring">
</center>

3. Install the corresponding propeller on each motor. The motors and propellers are labeled with a yellow or orange dot in addition to the markings "CW" and "CCW". These denote the rotation direction and must match.

<center>
  <img src="../img/IEV2_PropLabel.jpg" width="307" alt="IEV2 Propeller Labeling">
</center>

4. Install the fixing plate using the two provided hex head (M3 X 8mm) screws. Do not over-tighten.

<center>
  <img src="../img/IEV2_FixingPlate.jpg" width="307" alt="IEV2 Propeller Fixing Plate">
</center>

5. Inspect that all propellers are properly mounted.

#### Install Antennas (optional: only required for connecting to MissionPlanner/QGroundControl)
The UAS should have two antenna installed in the front. These antennas should be hand-tightened and angled at about 90º from one another.

<center>
  <img src="../img/IEV2_Antenna.jpg" width="307" alt="IEV2 Antenna Installation">
</center>

#### Connect wireless transmitter to PC (optional: only required for connecting to MissionPlanner/QGroundControl)
The UAS can connect with a mission planning software to transmit useful telemetry data.

1. Connect wireless transmitter labeled for IEV2 to the PC using a microUSB cable.
2. The mission planning software should automatically recognize the device and load telemetry data once the UAS is powered on.

#### Install WP_V2 Weather Tower (optional: only required for collecting weather data.)
The IEV2 is designed to carry the WP_V2 payload.

1. Using the provided thumb screws securely attach the WP_V2 to the IEV2.
2. Connect the XT-30 plug to power the WP_V2. The unit will book and automatically log data to the onboard SD card.

#### Install WP_V2 Wireless Transmitter (optional: only required to stream data to base station PC.)
*Note: when WP_V2 transmits data wirelessly, some data is missed during transmission. For best results, do not connect transmitter and just use SD card.*

1. Attach the wireless transmitter labeled for WP_V2 to the velcro near the WP_V2 control board.
2. Connect the wireless transmitter to the loose wire labeled "SER3."
3. Connect the provided short antenna to the transmitter.

<center>
  <img src="../img/WPV2_Transmitter.jpg" width="307" alt="WP_V2 Transmitter Installation">
</center>

#### Connect WP_V2 Wireless Receiver to PC (optional: only required if wireless transmitter in use on WP_V2)

1. Attach and angle (~90º) two antenna to the included RFD 900+ wireless receiver labeled as "WP_V2 Receiver".

<center>
  <img src="../img/WPV2_ReceiverAntenna.jpg" width="307" alt="WP_V2 Receiver Antenna">
</center>

2. Plug included USB into PC and connect other end to the RFD 900+ such that the pin 1 triangle marker (black wire) connect to pin 1 (bottom-left pin).

<center>
  <img src="../img/WPV2_ReceiverUSB.jpg" width="307" alt="WP_V2 Receiver USB Connection">
</center>

3. Open a serial viewer application such as [Realterm](https://realterm.sourceforge.io/), [PuTTY](https://www.chiark.greenend.org.uk/~sgtatham/putty/), [Tera Term](https://ttssh2.osdn.jp/index.html.en), or [CoolTerm](https://www.freeware.the-meiers.org/). Connect to the correct serial port ("COM##" on Windows or "usbserial-########" on MacOS).

4. Data should begin streaming once WP_V2 is connected. If data does not transmit, simply press the enter key in the serial monitor to initial data flow.


#### Connect RTK-GPS (optional: only required for GPS Precision Position Service (PPS))
The UAS can use a ground-based GPS reference for more precise location data.

1. Connect USB and antenna to RTK-GPS unit.

<center>
  <img src="../img/IEV2_RTK-GPS.jpg" width="307" alt="IEV2 RTK-GPS">
</center>

2. After initial set up of RTK-GPS following instructions in [Step-By-Step Guide to Add RTK Feature using QGroundControl](#step-by-step-guide-to-add-rtk-feature-using-qgroundcontrol) section, the RTK-GPS unit should automatically connect when connected to PC.
3. The RTK-GPS should be mounted high and away from any potential interferences. A small tower is a great option.
4. Allow the RTK-GPS to acquire satellite signal and survey in. The UAS will only use the RTK-GPS signal if the accuracy is < 2.00m.

#### Connect Transmitter
The transmitter automatically connects to the UAS once both are powered on. See [FrSky Taranis X9D Plus Controller Settings](#frsky-taranis-x9d-plus-controller-settings) for information on the flight modes.

### Arming UAS

1. Once all pre-arm checks are clear in the HUD on QGroundControl, press and hold the red illuminated safety switch on the Here GPS until the flight controller emits an audible tone.

<center>
  <img src="../img/IEV2_GPSArm.png" width="307" alt="Safety Switch">
</center>

2. With the transmitter set to the "IEV2 WX Stn" profile, the UAS can be armed using the following inputs for a few second until flight controller emits an audible tone:

- Switch SC: position 1 (Stabilize Mode) or position 2 (Loiter Mode).
- Switch SG: position 1 (not in RTL mode)
- Throttle: 0
- Yaw: full right

The ESCs will chirp and the rotors will begin to spin slowly and the UAS if ready for flight.

<center>
  <img src="../img/IEV2_TXArm.jpg" width="307" alt="IEV2 TX Arm Sequence">
</center>

### Fly!
With the unit armed, the UAS is ready to fly! The until can be flown using any of the three modes listed in [FrSky Taranis X9D Plus Controller Settings](#frsky-taranis-x9d-plus-controller-settings).

Note that when taking off in Loiter Mode, the UAS will need a throttle input greater than 50%. In Loiter Mode, 50% throttle keeps the UAS at a constant altitude - which is 0m when on the ground.

When ready to land, the drone can be landed in any mode. It will automatically disarm after being on the ground for a few seconds.

### Disarming UAS
The UAS will automatically disarm when landing in most modes (including RTL). However, should there be a need to manually disarm, the UAS can be disarmed using the following sequence:

1. Using the Taranis transmitter send the following inputs for a few second until flight controller emits an audible tone:

- Throttle: 0
- Yaw: full left

<center>
  <img src="../img/IEV2_TXDisarm.jpg" width="307" alt="IEV2 TX Disarm Sequence">
</center>

2. Press the safety switch on top of the GPS unit.

<center>
  <img src="../img/IEV2_GPSArm.png" width="307" alt="Safety Switch">
</center>

* * *

# Tasks and Nathan's Notes
- Set up a new profile to force drone to reach altitude with mast installed.
- Tuning: https://ardupilot.org/copter/docs/altholdmode.html
- Surface tracking (disable terrain): https://ardupilot.org/copter/docs/terrain-following-manual-modes.html#terrain-following-manual-modes
- Terrain Following: https://ardupilot.org/copter/docs/terrain-following.html
- Log file: https://ardupilot.org/copter/docs/logmessages.html#pos-canonical-vehicle-position
- WP_V2 does not seem to be outputting Trisonica data?!
  - I contacted Alex on 22 DEC 2021. I suspect that after upgrading the FW on the Trisonica, the output parameter changed. I've asked Alex to send a string to set the parameters.
- Drill USB plug into IEV2
