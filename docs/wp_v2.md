# WP_V2 Weather Tower

## Introduction
The WP_V2 Weather Tower is an optional attachment for the IEV2 UAS to collect weather data. The WP_V2 uses a custom circuit board to interface with the sensor payload and a [Teensy 3.5](https://www.pjrc.com/store/teensy35.html). The source code is located on [GitLab](https://gitlab.com/de-wekker-lab/emergentrc-intense-eye-v2/-/tree/master/ArduinoCode).

## Manual
WP_V2 Manual available on [GitLab](https://gitlab.com/de-wekker-lab/emergentrc-intense-eye-v2/-/blob/master/Manuals/WP-V2%20FINAL.pdf)

## Installation
See the following sections for information on mounting and connecting the WP_V2 to the IEV2:

1. [Install WP_V2 Weather Tower](../emergentrc/#install-wp_v2-weather-tower-optional-only-required-for-collecting-weather-data).
2. [Install WP_V2 Wireless Transmitter](../emergentrc/#install-wp_v2-wireless-transmitter-optional-only-required-to-stream-data-to-base-station-pc).
3. [Connect WP_V2 Wireless Receiver to PC](../emergentrc/#connect-wp_v2-wireless-receiver-to-pc-optional-only-required-if-wireless-transmitter-in-use-on-wp_v2).

## Programming Teensy 3.5
The [Teensy 3.5](https://www.pjrc.com/store/teensy35.html) data logger main code is hosted on [GitLab](https://gitlab.com/de-wekker-lab/emergentrc-intense-eye-v2/-/tree/master/ArduinoCode).

### Interfacing directly with the Trisonica through the Teensy 3.5
The Trisonica Mini will occasionally require firmware updates. This can be accomplished directly through the Teensy 3.5 board. By programming the Teensy 3.5 with a serial passthrough code, the Trisonica can be accessed directly.

1. Download the compiled serial passthrough hex code located on [GitLab](https://gitlab.com/de-wekker-lab/emergentrc-intense-eye-v2/-/blob/master/ArduinoCode/TrisonicaSerialPassthrough.ino.hex).
2. Download the [Teensy Loader Application](https://www.pjrc.com/teensy/loader.html)
3. Power the Teensy 3.5 through the WP_V2 carrier board (it will not be powered by the USB alone).
4. Open the Teensy Loader Application, select *File>Open HEX File* and select the TrisonicaSerialPassthrough.ino.hex file.
5. Press the white button to run HalfKay. This should automatically load the code and restart the Teensy 3.5.

The Teensy 3.5 should now be accessible by any terminal application. Interact with the Trisonica just as if it were connected directly to PC via RS232.

### Loading Main WP_V2 Code onto Teensy 3.5

1. Download the compiled serial passthrough hex code located on [GitLab](https://gitlab.com/de-wekker-lab/emergentrc-intense-eye-v2/-/blob/master/ArduinoCode/WP_V2_MCP9600.ino.hex).
2. Download the [Teensy Loader Application](https://www.pjrc.com/teensy/loader.html)
3. Power the Teensy 3.5 through the WP_V2 carrier board (it will not be powered by the USB alone).
4. Open the Teensy Loader Application, select *File>Open HEX File* and select the WP_V2_MCP9600.ino.hex file.
5. Press the white button to run HalfKay. This should automatically load the code and restart the Teensy 3.5.

* * *

# Tasks and Nathan's Notes
- Set up a new profile to force drone to reach altitude with mast installed.
- Tuning: https://ardupilot.org/copter/docs/altholdmode.html
- Surface tracking (disable terrain): https://ardupilot.org/copter/docs/terrain-following-manual-modes.html#terrain-following-manual-modes
- Terrain Following: https://ardupilot.org/copter/docs/terrain-following.html
- Log file: https://ardupilot.org/copter/docs/logmessages.html#pos-canonical-vehicle-position
- WP_V2 does not seem to be outputting Trisonica data?!
  - I contacted Alex on 22 DEC 2021. I suspect that after upgrading the FW on the Trisonica, the output parameter changed. I've asked Alex to send a string to set the parameters.
- Drill USB plug into IEV2
