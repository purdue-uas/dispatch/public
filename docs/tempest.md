# Tempest Station 2 Data

<div>
  <center><iframe id="wind" width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQOFukbR8DNOyfpjn9ca9dTpff25l-tMCf2WzxKOVgqfzSeSfzchuXoTqGKy2IhUQTdQnhZSU1XvoDx/pubchart?oid=5764334&amp;format=interactive"></iframe></center>

<p><center><iframe id="temp" width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQOFukbR8DNOyfpjn9ca9dTpff25l-tMCf2WzxKOVgqfzSeSfzchuXoTqGKy2IhUQTdQnhZSU1XvoDx/pubchart?oid=709364056&amp;format=interactive"></iframe></center>

<p><center><iframe id="rain" width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQOFukbR8DNOyfpjn9ca9dTpff25l-tMCf2WzxKOVgqfzSeSfzchuXoTqGKy2IhUQTdQnhZSU1XvoDx/pubchart?oid=63040041&amp;format=interactive"></iframe></center>

<p><center><iframe id="grid" width="600" height="1400" seamless frameborder="0" scrolling="no" src="https://tempestwx.com/station/47226/grid"></iframe></center>

<script>
window.setInterval("reloadIFrame();", 60000);
function reloadIFrame() {
 document.getElementById("wind").src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQOFukbR8DNOyfpjn9ca9dTpff25l-tMCf2WzxKOVgqfzSeSfzchuXoTqGKy2IhUQTdQnhZSU1XvoDx/pubchart?oid=5764334&amp;format=interactive";
document.getElementById("temp").src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQOFukbR8DNOyfpjn9ca9dTpff25l-tMCf2WzxKOVgqfzSeSfzchuXoTqGKy2IhUQTdQnhZSU1XvoDx/pubchart?oid=709364056&amp;format=interactive";
document.getElementById("rain").src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQOFukbR8DNOyfpjn9ca9dTpff25l-tMCf2WzxKOVgqfzSeSfzchuXoTqGKy2IhUQTdQnhZSU1XvoDx/pubchart?oid=63040041&amp;format=interactive";
document.getElementById("grid").src="https://tempestwx.com/station/47226/grid";
}
</script>
</div>

## Links of interest
[Using RPi in AP mode](https://blog.thewalr.us/2017/09/26/raspberry-pi-zero-w-simultaneous-ap-and-managed-mode-wifi/)
[Arduino Serial Buffer](https://www.gammon.com.au/serial)
[Arduino Interrupts](https://www.gammon.com.au/blink)
