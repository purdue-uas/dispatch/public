# Availability

[Outlook Calendar](https://outlook.office365.com/owa/calendar/ccc69dcfa6b341fd92f516ae393e5836@purdue.edu/6d6b81d342924ff19e8edd3e16ff0f2f8031924016254706271/calendar.html)
<iframe src="https://outlook.office365.com/owa/calendar/ccc69dcfa6b341fd92f516ae393e5836@purdue.edu/6d6b81d342924ff19e8edd3e16ff0f2f8031924016254706271/calendar.html" title="UAS Dispatch Availability" width="100%" height="1000"></iframe> 

* * *

👋 Notice something off? Want to make a change? Submit an [issue](https://gitlab.com/purdue-uas/dispatch/public/-/issues/new), email <a href="mailto:UASdispatch@purdue.edu, nathanrose@purdue.edu">UAS Dispatch Team</a>, or [fix it](https://gitlab.com/purdue-uas/dispatch/public) yourself.
