# Tasks
- [ ] Change oil in Generator
- [ ] [Spark plug BPR6HS (NGK)](https://mtp-racing24.com/Spark-Plug-NGK-BPR6HS) with 21mm wrench
- [x] Setup RTK GPS on IEV2
- [x] Look into configuring [VTOL Weather Vane Feature](https://docs.px4.io/master/en/config_vtol/vtol_weathervane.html)
- [ ] Purchase for RevP:
    *  [ ] [Screws](https://www.mcmaster.com/screws/socket-head-screws/socket-head-screws-6/threading~fully-threaded/thread-direction~right-hand/material~stainless-steel/material~18-8-stainless-steel/thread-size~m3/thread-it-screw-bolt-and-nut-size-identifiers-6/) (see email from Ken)
    * [ ] [Lanyard Supports](https://www.mcmaster.com/catalog/127/1613)
    * [ ] [Lanyard Kit](https://www.mcmaster.com/lanyards/create-your-own-lanyards-not-for-lifting/)
    * [ ] [Eyebolts for Lanyard](https://www.mcmaster.com/eyebolts/)
    * [ ] [Rail System](https://www.mcmaster.com/1935N56/)
    * [ ] [Pipe Clamps](https://www.mcmaster.com/pipe-clamps/)
    * [ ] [DragonPlate](https://dragonplate.com/05-Male-Clevis-Connector)
    * [ ] [RockWest Composite](https://www.rockwestcomposites.com/shop/connector-accessories/carbonnect/carbonnect-system-components/main-blocks-and-adapters/ce-mb4-05-group)
- [ ] WeatherFlow Links
    * [ ] [WallGauge](https://github.com/WallGauge-GaugeApps/weatherFlowDataGetter)
    * [ ] [Tempest API](https://weatherflow.github.io/Tempest/api/)
    * [ ] [Bike Wind Sensor](https://cdacrr.blogspot.com/p/modes.html)
    * [ ] [WeatherFlowHK Bridge](https://github.com/noblecloud/WeatherFlowHK)
    * [ ] [WeeWx](https://www.wxforum.net/index.php?topic=34056.0)
    * [ ] [IFTTT to Google Doc](https://ifttt.com/applets/tTaBNPGw-log-tempest-data-to-google-sheets)
    * [ ] [DataScope](https://ds.weatherflow.com/login.aspx)
    * [ ] [Grafana and InfluxDB](https://grafana.com/grafana/dashboards/13857)
    * [ ] [Integrations](https://help.weatherflow.com/hc/en-us/articles/115005229547-Integrations)
    * [ ] [Arduino UDP](http://dewan.info/2021/03/20/arduino-based-weatherflow-display/)
    * [ ] [InfluxDB](https://community.weatherflow.com/t/weatherflux-an-influxdb-2-x-integration-gateway-for-weatherflow-stations/11798/4)

- [ ] https://tempestwx.com/station/26407/
- [ ] https://weatherflow.com/wind-meter/
- [ ] https://cdacrr.blogspot.com/
- [ ] https://fttechnologies.com/support/installation/surface-mount-sm/
- [ ] https://fttechnologies.com/case-studies/urban-air-mobility-kosovo/

- [X] Follow up with Calypso RE: New FW with lower cut off threshold and higher resolution.
- [ ] [FT Sonics](https://fttechnologies.com/support/installation/surface-mount-sm/)
