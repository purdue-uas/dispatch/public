<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.collapsible {
  background-color: #777;
  color: white;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
}

.active, .collapsible:hover {
  background-color: #555;
}

.content {
  padding: 0 18px;
  display: none;
  overflow: hidden;
  background-color: #f1f1f1;
}
</style>
</head>
<body>

<h2>Help</h2>

<p>Dispatch Catalog:</p>
<button type="button" class="collapsible">How do I open the site?</button>
<div class="content">
  <p>Just click <a href="https://www.librarycat.org/lib/Purdue_UAS_Dispatch">here</a>.</p>
</div>
<button type="button" class="collapsible">How do I request a drone?</button>
<div class="content">
  <p> <ol>
  <li>Search for the drone you wish to check out. For example, search <a href="https://www.librarycat.org/lib/Purdue_UAS_Dispatch/search/text/mavic">'mavic'</a>.</li>
  <li>Click "Place Hold" on any drone that matches what you are looking for and is AVAILABLE.</li>
  <li>Enter your "Patron ID" when prompted. This is your Purdue Computing ID.</li>
  <li>Click "Sign in", then "Conform request".</li>
  <li>This will place the drone on hold for 24 hours and will alert the dispatch to prepare the drone.</li>
</ol> </p>
</div>
<button type="button" class="collapsible">Okay... But SHOW me how I request a drone?</button>
<div class="content">
  <p><iframe width="560" height="315" src="https://www.youtube.com/embed/JhHw2ZUHl3Q?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
</div>

<button type="button" class="collapsible">My emails are going to Junk Mail</button>
<div class="content">
  <p>Purdue IT has implemented new junk mail filters and refuses to add the librarything.com domain to the global whitelist. Instead, each individual user will have to add the domain to their Safe Senders list manually following the steps below.
  <p> <ol>
    <li>Log into the online version of <a href="https://outlook.office.com/mail/">outlook</a>.
    <p><img src="../img/help/junk01.png" alt="OWA Settings"></p>
    <li>Click Mail > Junk Email.
    <li>Scroll down to Safe senders and domains.
    <li>Click + and type <b>librarything.com</b>
    <li>Press the enter key and click save to apply the setting.
    <p><img src="../img/help/junk02.png" alt="Settings Dialog"></p>
  </p> </ol>
</div>

<script>
var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}
</script>

</body>
</html>


* * *

👋 Notice something off? Want to make a change? Submit an [issue](https://gitlab.com/purdue-uas/dispatch/public/-/issues/new), email <a href="mailto:UASdispatch@purdue.edu, nathanrose@purdue.edu">UAS Dispatch Team</a>, or [fix it](https://gitlab.com/purdue-uas/dispatch/public) yourself.
