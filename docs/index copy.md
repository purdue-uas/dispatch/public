# Purdue UAS

<center>
  <img src="img/index_logo.png" width="300" alt="Purdue SATT Logo">
</center>

This is the internet home of all things Purdue UAS. Please use the links to the left to navigate to the UAS resources. 

## Contents
### Flight Planning
  [METAR & TAF (KLAF)](https://www.aviationweather.gov/taf/data?ids=klaf&format=raw&metars=on&layout=on)
  [Flight Areas](flight_areas.md)
  [Dispatch System](https://www.librarycat.org/lib/Purdue_UAS_Dispatch)
### Logs and Records
  [Aircraft Registrations](uas_registrations.md)
  [Contact Us](contact.md)
  [Internal Site (login required)](https://purdue-uas.gitlab.io/dispatch/internal/)

## Photos

<center>
<figure>
  <img src="img/index_at219_zach.jpg" width="500" alt="AT 219 Zach">
  <figcaption><small><small><br>Zachary Ryan post-processing his 3D printed vibration damping flight controller mount.</small></small></figcaption>
</figure>
<figure>
  <img src="img/index_at219_ben.jpg" width="500" alt="AT 219 Benjamin">
  <figcaption><small><small><br>Benjamin Jenness tunes the 3D printer before starting his GPS mount.</small></small></figcaption>
</figure>
<figure>
  <img src="img/index_pad_dutchCameron.jpg" width="500" alt="AT 219 Dutch and Cameron">
  <figcaption><small><small><br>Dutch Byrd and Cameron Wingren demonstrating the capabilities of the DJI Matrice 300 sensor packages.</small></small></figcaption>
</figure>
</center>


* * *

👋 Notice something off? Wanna make a change? Submit an [issue](https://gitlab.com/purdue-uas/dispatch/public/-/issues/new), email <a href="mailto:UASdispatch@purdue.edu, nathanrose@purdue.edu">UAS Dispatch Team</a>, or [fix it](https://gitlab.com/purdue-uas/dispatch/public) yourself.
