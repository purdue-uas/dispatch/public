# DJI D-RTK 2 Setup

This is an in-depth guide to setting up the DJI D-RTK 2 with the DJI M300 RTK.

**Items Needed**

- DJI D-RTK Body
- DJI D-RTK 2 Base Station Tripod
- Intelligent Battery (WB37 - Controller Batteries)
- DJI M300 RTK

## Assembly (Change Maybe)

1. Setup the D-RTK 2 Base Station Tripod and use the leveling bubble on the tripod to ensure it is level.
   - Make sure the center carbon-fiber extension pole is touching the ground.
   - Tighten the lock nut on the carbon-fiber extension pole to ensure it is secure.
2. Insert the battery in D-RTK 2 Body.
   - To remove the battery cover, first pull it down and then pull it off.
   - To attach the battery cover, first push in and then push up.   
3. Attach the D-RTK 2 Body to the Tripod and tighten the lock nut to secure the Body onto the Tripod.
   - Use the leveling bubble on the D-RTK 2 Body to ensure it is level.

Once these steps are completed, the D-RTK 2 is ready to be powered on and configured.

## Start Up & Configuration

1. Turn on the D-RTK 2 by press and holding the power button
   - This is the center button on the D-RTK 2 (*see figure 1*)

<i> 

<div style = "text-align:center;">
<img src="../img/rtk_imgs/powerbutton.png" wide="300" height="300">
<figcaption>Figure 1: DJI D-RTK 2 Power Button</figcaption>
</div>
</i>

2. Once the D-RTK 2 is on, it will take a few minutes to initialize and start up. 
   - The indicator under the power button will display the amount of Satellites the D-RTK 2 units is connected to (see *figure 2*)

<i>
<div style = "text-align:center;">
<img src="../img/rtk_imgs/powerindicator.png" wide="200" height="150">
<figcaption>Figure 2: DJI D-RTK 2 Power Indicator Descriptions</figcaption>
</div> 
</i>

3. Next, you will want to make sure that the D-RTK 2 unit is in the correct operating mode (for the M300 this is mode 5).
   
   - This can be determined by how many times the light flashes on the operating mode indicator, which is the button/ indicator to the right of the Power Button.
   - If you need to change the mode, follow the steps located on the DJI D-RTK 2 Body.

4. Once the D-RTK 2 unit is in the correct mode, power on the M300 and within the RTK settings menu toggle on *RTK Positioning* (*see figure 3*).

<i> 

<div style = "text-align:center;">
<img src="../img/rtk_imgs/rtk_on.png" wide="300" height="300">
<figcaption>Figure 3: RTK Positioning Turned On</figcaption>
</div>  
</i>

5. You can tell if the M300 is receiving data from the D-RTK 2 unit if the *RTK* in the top right corner of the screen is WHITE (see *figure 4*).
   - If the *RTK* is RED but the satellites are still showing up in white then that means the M300 is using its own GPS.
   - If the RTK is not connected to the M300, navigate back into the RTK settings menu and click on *status* to reconnect.

<i> 

<div style = "text-align:center;">
<img src="../img/rtk_imgs/rtksatellite.png" wide="300" height="300">
<figcaption>Figure 4: RTK in Top Right Corner of Screen Showing up as WHITE</figcaption>
</div>  
</i>

Once you have ensured that the M300 is receiving data from the D-RTK 2, you can begin your flight.  
    - If you have a known (surveyed) point, you are able to enter those coordinates manually and have the D-RTK 2 correct from the known point. The instructions for that are listed below.

### Inputting known Coordinates

Before inputting your own coordinates, make sure the center carbon-fiber extension pole is on the exact point that the surveyed point was placed. If not, this could cause your corrections to be off.

1. To input your own coordinates, first scroll down to the bottom of the RTK page and select the Advance Settings tab (see *figure 5*).

<i> 

<div style = "text-align:center;">
<img src="../img/rtk_imgs/advancesettings.png" wide="300" height="300">
<figcaption>Figure 5: Advance Settings is located at the bottom of the RTK Setting Page</figcaption>
</div>
</i>

2. After selecting *advanced settings*, the pilot 2 app will require a password, which is "123456" 
   
   - ***Please do not change the password as it will ruin the documentation/ the ability for everyone else to successfully use the D-RTK 2 Unit***  

3. In the advanced settings tab, click Adjust Coordinates and then you will be able to input the known longitude, latitude, and Ellipsoidal Height (see *figure 6*).

<i> 

<div style = "text-align:center;">
<img src="../img/rtk_imgs/editcoordinates.png" wide="300" height="300">
<figcaption>Figure 6: Edit Longitude/ Latitude & Ellipsoidal Height</figcaption>
</div>
</i>

<ins>NOTICE</ins>  
After entering the known coordinate, the M300 may give a warning that states *"Sudden RTK Position Data Change"*. To fix this, after you have entered the known coordinates, restart the **M300** only (not the D-RTK 2). Once the M300 restarts, the warning should be fixed and you will be ready to fly with that known coordinates in the D-RTK 2 system. 
