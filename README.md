# Purdue UAS Dispatch

[![pipeline 
status](https://gitlab.com/purdue-uas/dispatch/public/badges/master/pipeline.svg)](https://gitlab.com/purdue-uas/dispatch/public/-/commits/master)

## Introduction

This is the repository and GitLab runner for the UAS Dispatch wiki protected page located at https://purdue-uas.gitlab.io/dispatch/wiki/ This site will host all of the documentation for UAS Dispatch operations.

## Make Edits

#### 1. Gain Access

To make edits to this website, you must first be a member of the repository. Email <a href="mailto:rose196@purdue.edu?subject=Dispatch GitLab Access Request">Nathan Rose</a> for access.

#### 2. Setup Git and GitLab access.

If you are unexperienced with Git and GitLab, feel free to follow [this tutorial](https://gitlab.com/purdue-uas/at21900/course-material/-/blob/main/week1/lab1/README.md) to get setup. 

#### 3. Clone the Repository

In a terminal application ([PowerShell](https://docs.microsoft.com/en-us/powershell/) or [iTerm2](https://iterm2.com))
